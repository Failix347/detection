#include "FAST.h"



FAST::FAST()
{
	_intensity_threshold_int = 30;
	_intensity_threshold_f = 0.4f;
	_min_keypoint_distance = 16.0f;
	_suppression_optimisation_number = 40;
	_indices = { pair<int,int>(0,4),pair<int,int>(1,4),pair<int,int>(3,3),pair<int,int>(4,1),pair<int,int>(4,0),pair<int,int>(4,-1),pair<int,int>(3,-3),pair<int,int>(1,-4),
		pair<int,int>(0,-4),pair<int,int>(-1,-4),pair<int,int>(-3,-3),pair<int,int>(-4,-1),pair<int,int>(-4,0),pair<int,int>(-4,1),pair<int,int>(-3,3),pair<int,int>(-1,4) };
}


FAST::~FAST()
{
}

void FAST::detect(const Mat& image, vector<KeyPoint>& keypoints)
{
	Mat grayscale;
	cvtColor(image, grayscale, COLOR_RGB2GRAY);

	auto time_start_detection = chrono::high_resolution_clock::now();
	for (int x = 4; x < (grayscale.rows - 4); ++x)
	{
		for (int y = 4; y < (grayscale.cols - 4); ++y)
		{
			if (IsInterestPointCorner(x, y, grayscale))
			{
				keypoints.push_back(KeyPoint(y,x, 7.0f, -1.0f, 0, 0, 0));
			}
		}
	}
	cout << "\n Size before:" << keypoints.size();
	auto time_start = chrono::high_resolution_clock::now();
	NonMaxSuppression(grayscale, keypoints);
	auto time_end = chrono::high_resolution_clock::now();
	cout << "\n Size after:" << keypoints.size();
	cout << "\n NonMaxSuppression Time:" << chrono::duration_cast<chrono::milliseconds>(time_end - time_start).count() << "ms";
	cout << "\n Cycle Time:" << chrono::duration_cast<chrono::milliseconds>(time_end - time_start_detection).count() << "ms \n";
}



bool FAST::IsInterestPoint(const int x, const int y, Mat& image)
{
	int pixels_over_threshold = 0;
	auto intensity = (int)image.at<uchar>(x, y);
	
	/*
	for (int i = 0; i < 16; i += 4) {
		if (ComparePixelWithT(x+_indexes[i].first, y + _indexes[i].second, intensity, image))++pixels_over_threshold;
	}

	if (pixels_over_threshold < 3) return false;

	for (int i = 0; i < 16; ++i) {
		if (i % 4 != 0) {
			if (ComparePixelWithT(x + _indexes[i].first, y + _indexes[i].second, intensity, image))++pixels_over_threshold;
		}
	}*/
	
	if (ComparePixelWithT(x, y + 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 4, y, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x, y - 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 4, y, intensity, image))++pixels_over_threshold;

	if (pixels_over_threshold < 3) return false;

	if (ComparePixelWithT(x + 1, y + 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 3, y + 3, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 4, y + 1, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 4, y - 1, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 3, y - 3, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 1, y - 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 1, y - 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 3, y - 3, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 4, y - 1, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 4, y + 1, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 3, y + 3, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 1, y + 4, intensity, image))++pixels_over_threshold;

	return pixels_over_threshold >= _contiguous_pixels;
}

bool FAST::IsInterestPointCorner(const int x, const int y, Mat & image)
{
	int pixels_over_threshold = 0;
	int start_pixel = -1;
	auto intensity = (int)image.at<uchar>(x, y);

	/*for (int i = 0; i < 16; i += 4) {
		if (ComparePixelWithT(x + _indexes[i].first, y + _indexes[i].second, intensity, image))++pixels_over_threshold;
	}

	if (pixels_over_threshold < 3) return false;
	pixels_over_threshold = 0;

	for (int i = 0; i < 16; ++i) 
	{
		if (ComparePixelWithT(x + _indexes[i].first, y + _indexes[i].second, intensity, image))
		{
			++pixels_over_threshold;
			if (start_pixel > -1)start_pixel = i;
		}
		else
		{
			if (i > 4)
			{
				break;
			}
			else
			{
				start_pixel = -1;
				pixels_over_threshold = 0;
			}
		}
	}*/

	
	if (ComparePixelWithT(x, y + 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x + 4, y, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x, y - 4, intensity, image))++pixels_over_threshold;
	if (ComparePixelWithT(x - 4, y, intensity, image))++pixels_over_threshold;

	if (pixels_over_threshold < 3) return false;

	pixels_over_threshold = 0;
	int first_false = -1;

	if (ComparePixelWithT(x + 0, y + 4, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 0 : first_false; }
	if (ComparePixelWithT(x + 1, y + 4, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 1 : first_false; }
	if (ComparePixelWithT(x + 3, y + 3, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 2 : first_false; }
	if (ComparePixelWithT(x + 4, y + 1, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 3 : first_false; }
	if (ComparePixelWithT(x + 4, y + 0, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 4 : first_false; }
	if (ComparePixelWithT(x + 4, y - 1, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 5 : first_false; }
	if (ComparePixelWithT(x + 3, y - 3, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 6 : first_false; }
	if (ComparePixelWithT(x + 1, y - 4, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 7 : first_false; }
	if (ComparePixelWithT(x + 0, y - 4, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 8 : first_false; }
	if (ComparePixelWithT(x - 1, y - 4, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 9 : first_false; }
	if (ComparePixelWithT(x - 3, y - 3, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 10 : first_false; }
	if (ComparePixelWithT(x - 4, y - 1, intensity, image))++pixels_over_threshold; else { pixels_over_threshold = 0; first_false = first_false == -1 ? 11 : first_false; }
	if (ComparePixelWithT(x - 4, y + 0, intensity, image))++pixels_over_threshold; else { if (pixels_over_threshold >= _contiguous_pixels) { return true; } else { pixels_over_threshold = 0; first_false = first_false == -1 ? 12 : first_false; } }
	if (ComparePixelWithT(x - 4, y + 1, intensity, image))++pixels_over_threshold; else { if (pixels_over_threshold >= _contiguous_pixels) { return true; } else { pixels_over_threshold = 0; first_false = first_false == -1 ? 13 : first_false; } }
	if (ComparePixelWithT(x - 3, y + 3, intensity, image))++pixels_over_threshold; else { if (pixels_over_threshold >= _contiguous_pixels) { return true; } else { pixels_over_threshold = 0; first_false = first_false == -1 ? 14 : first_false; } }
	if (ComparePixelWithT(x - 1, y + 4, intensity, image))++pixels_over_threshold; else { if (pixels_over_threshold >= _contiguous_pixels) { return true; } else { pixels_over_threshold = 0; first_false = first_false == -1 ? 15 : first_false; } }

	

	return (pixels_over_threshold + first_false) >= _contiguous_pixels;
}

bool inline FAST::ComparePixelWithT(int x, int y, const int intensity, Mat& image)
{
	/*int i = (int)(image.at<uchar>(x, y));
	int diff = abs(intensity - i);
	float coeff = 0.0f;
	if (intensity == 0) coeff = (float)diff / 0.00001f;
	else coeff = (float)diff / (float)intensity;
	return coeff > _intensity_threshold_f;*/
	return abs(intensity - (int)(image.at<uchar>(x, y)))>_intensity_threshold_int;
}

int FAST::ScoreFunction(const int x, const int y, Mat & image)
{
	int score = 0;
	auto intensity = (int)image.at<uchar>(x, y);

	score += ComputeDifference(x, y + 4, intensity, image);
	score += ComputeDifference(x + 4, y, intensity, image);
	score += ComputeDifference(x, y - 4, intensity, image);
	score += ComputeDifference(x - 4, y, intensity, image);
	score += ComputeDifference(x + 1, y + 4, intensity, image);
	score += ComputeDifference(x + 3, y + 3, intensity, image);
	score += ComputeDifference(x + 4, y + 1, intensity, image);
	score += ComputeDifference(x + 4, y - 1, intensity, image);
	score += ComputeDifference(x + 3, y - 3, intensity, image);
	score += ComputeDifference(x + 1, y - 4, intensity, image);
	score += ComputeDifference(x - 1, y - 4, intensity, image);
	score += ComputeDifference(x - 3, y - 3, intensity, image);
	score += ComputeDifference(x - 4, y - 1, intensity, image);
	score += ComputeDifference(x - 4, y + 1, intensity, image);
	score += ComputeDifference(x - 3, y + 3, intensity, image);
	score += ComputeDifference(x - 1, y + 4, intensity, image);

	return score;
}

int inline FAST::ComputeDifference(const int x, const int y, const int intensity, Mat & image)
{
	/*int i = (int)(image.at<uchar>(x, y));
	int diff = abs(intensity - i);*/
	return abs(intensity - (int)(image.at<uchar>(x, y)));
}

float inline FAST::PointDistanceSquared(const int x1, const int y1, const int x2, const int y2)
{
	return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
}

void FAST::NonMaxSuppression(Mat & image, vector<KeyPoint>& keypoints)
{
	std::vector<bool> toDelete(keypoints.size());
	std::vector<int> scores(keypoints.size());
	for (auto it = keypoints.begin(); it < keypoints.end(); ++it)
	{
		if (!toDelete[distance(keypoints.begin(), it)])
		{
			scores[distance(keypoints.begin(), it)] = ScoreFunction(it->pt.y, it->pt.x, image);
			int out_of_range_since_last_found = 0;

			for (auto in = it + 1; in < keypoints.end(); ++in)
			{
				//stop if we haven't been in range of a point for a number of comparisons
				if (out_of_range_since_last_found > _suppression_optimisation_number)break;
				if (!toDelete[distance(keypoints.begin(), in)])
				{
					if (PointDistanceSquared(it->pt.x, it->pt.y, in->pt.x, in->pt.y) < _min_keypoint_distance)
					{
						//compute and store score for a point when it is accessed for the first time
						if (scores[distance(keypoints.begin(), in)] == 0)
						{
							scores[distance(keypoints.begin(), in)] = ScoreFunction(in->pt.y, in->pt.x, image);
						}

						if (scores[distance(keypoints.begin(), it)] < scores[distance(keypoints.begin(), in)])
						{
							toDelete[distance(keypoints.begin(), it)] = true;
							out_of_range_since_last_found = 0;
							break;
						}
						else
						{
							toDelete[distance(keypoints.begin(), in)] = true;
							out_of_range_since_last_found = 0;
						}
					}
					else
					{
						++out_of_range_since_last_found;
					}
				}
			}
			
		}
	}
	auto it = keypoints.end()-1;
	for (int i = toDelete.size() - 1; i >= 0; --i) 
	{
		if (it > keypoints.begin()) {
			--it;
			if (toDelete[i]) {
				keypoints.erase(it + 1);
			}
		}
		else
		{
			if (toDelete[i]) {
				keypoints.erase(it);
			}
		}

	}
}

