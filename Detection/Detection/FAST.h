#pragma once

#include "opencv2\opencv.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "chrono"

using namespace cv;
using namespace std;
class FAST
{
public:
	FAST();
	~FAST();

	void detect(const Mat& image, vector<KeyPoint>& keypoints);

private:
	const int _contiguous_pixels = 12;
	int _intensity_threshold_int;
	float _intensity_threshold_f;
	float _min_keypoint_distance;
	int _suppression_optimisation_number;
	vector<pair<int, int>>  _indices;


	bool IsInterestPoint(const int x, const int y, Mat& image);
	bool IsInterestPointCorner(const int x, const int y, Mat& image);
	bool inline ComparePixelWithT(const int x, const int y, const int intensity, Mat& image);

	int ScoreFunction(const int x, const int y, Mat& image);
	int inline ComputeDifference(const int x, const int y, const int intensity, Mat& image);
	float inline PointDistanceSquared(const int x1, const int y1, const int x2, const int y2);
	void NonMaxSuppression(Mat& image, vector<KeyPoint>& keypoints);

};

