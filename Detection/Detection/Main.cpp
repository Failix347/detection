#include "SIFT.h"


int main(int argc, char** argv)
{
	bool paused = false;
	VideoCapture cap = VideoCapture(CAP_DSHOW);
	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);

	Mat img = imread("C:/Users/felix.schmid/Downloads/Profilbild.jpg", IMREAD_COLOR);
	// open the default camera, use something different from 0 otherwise;
	// Check VideoCapture documentation.
	if (!cap.open(0))
		return 0;
	while (true) {
		if (!paused)
		{
			Mat frame;
			cap >> frame;


			if (img.empty()) return 0; // end of video stream
			imshow("this is you, smile! :)", img);
			if (waitKey(10) == 1);
			paused = !paused;
		}
		else
		{
			if (waitKey(10) == 100) paused = !paused; // stop capturing by pressing ESC 
		}
	}
	// the camera will be closed automatically upon exit
	// cap.close();
	return 0;
}