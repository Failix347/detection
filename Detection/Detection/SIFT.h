#include "opencv2\opencv.hpp"

using namespace cv;
using namespace std;

class SIFT
{
public:
	SIFT();
	~SIFT();


private:
	void CreateScaleSpace(Mat& img);


	int number_octaves;
	int scales_per_octave;
	int sampling_distanec;
	float minimum_blur;
	float input_blur;
};
