#pragma once

#include "opencv2/opencv.hpp"
#include <opencv2/features2d.hpp>


using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	VideoCapture cap;
	bool pause = false;
	int selector = 0;
	vector<KeyPoint> keypointsFast;
	Ptr<FastFeatureDetector> detector = FastFeatureDetector::create();
	vector<Mat> descriptor;
	/*vector<KeyPoint> keypointsSift;
	Ptr<SiftFeatureDetector> detector_sift = new SiftFeatureDetector(
		0, // nFeatures
		4, // nOctaveLayers
		0.04, // contrastThreshold
		10, //edgeThreshold
		1.6 //sigma
	);*/
	// open the default camera, use something different from 0 otherwise;
	// Check VideoCapture documentation.
	if (!cap.open(0))
		return 0;
	for (;;)
	{
		if (!pause)
		{
			Mat frame;
			cap >> frame;
			int x = 300;
			int y = 140;
			
			//detector_sift->detect(frame, keypointsSift, Mat());
			Mat cropedImage = frame(Rect(x, y, 267, 200));


			detector->detect(cropedImage, keypointsFast, Mat());
			switch (selector) {
			case 0:
				drawKeypoints(cropedImage, keypointsFast, cropedImage);
				if (cropedImage.empty()) return 0;// break; // end of video stream
				imshow("FAST :)", cropedImage);
				break;
			case 1:
				//drawKeypoints(frame, keypointsSift, frame);
				if (frame.empty()) return 0;// break; // end of video stream
				imshow("Sift :)", frame);
				break;
			}
			

			if (waitKey(10) == 27) pause = !pause;//break; // stop capturing by pressing ESC 
			//pause = !pause;
		}
		else
		{
			if (waitKey(10) == 27) 
			{
				pause = !pause;
				//if (selector == 0)selector = 1;
				//else selector = 0;
			}
		}
	}
	// the camera will be closed automatically upon exit
	// cap.close();
	return 0;
}