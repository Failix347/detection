#include "GaussianBlur.h"



GaussianBlur::GaussianBlur()
{
}


GaussianBlur::~GaussianBlur()
{
}

void GaussianBlur::BlurColor(Mat & input, Mat & output, const float sigma, const int radius)
{
	vector<float> weights = GetKernelWeights(sigma, radius);


}

vector<float> GaussianBlur::GetKernelWeights(float sigma, int radius)
{
	vector<float> result;
	float sum = 0;
	for (int i = 0; i < radius; ++i) 
	{
		auto tmp = -pow((i - (radius - 1) / 2), 2);
		tmp = tmp / (2 * sigma *sigma);
		result[i] = exp(tmp);
		if(i==0)
		{
			sum = result[i];
		}
		else
		{
			sum += 2 * result[i];
		}
	}
	for (int i = 0; i < result.size(); ++i)
	{
		result[i] /= sum;
	}
	return result;
}

Vec3b GaussianBlur::SamplePointX(Mat & input, const int x, const int y, vector<float> weights)
{
	Vec3b result = input.at<Vec3b>(y, x);
	result = result*weights[0];

	for (int i = 1; i < weights.size();++i) 
	{
		if (x - i > 0)
		{
			result += input.at<Vec3b>(y, x - i)*weights[i];
		}
		if (x + i < input.cols)
		{
			result += input.at<Vec3b>(y, x + i)*weights[i];
		}
	}

	return result;
}

Vec3b GaussianBlur::SamplePointY(Mat & input, const int x, const int y, vector<float> weights)
{
	Vec3b result = input.at<Vec3b>(y, x);
	result = result*weights[0];

	for (int i = 1; i < weights.size(); ++i)
	{
		if (y - i > 0)
		{
			result += input.at<Vec3b>(y - i, x)*weights[i];
		}
		if (y + i < input.rows)
		{
			result += input.at<Vec3b>(y + i, x)*weights[i];
		}
	}

	return result;
}