#pragma once
#include "vector"
#include "math.h"
#include "opencv2\opencv.hpp"

using namespace std;
using namespace cv;

class GaussianBlur
{
public:
	GaussianBlur();
	~GaussianBlur();

	void BlurColor(Mat & input, Mat & output, const float sigma,const int radius );

private:
	vector<float> GetKernelWeights(float sigma, int radius);
	Vec3b SamplePointX(Mat & input, const int x, const int y, vector<float> weights);
	Vec3b SamplePointY(Mat & input, const int x, const int y, vector<float> weights);


};

